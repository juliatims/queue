import React from 'react';

import style from './style.css';
import Card from '@material-ui/core/Card/Card';
import Input from '../../components/simple/input/input';
import SelectQueue from '../../components/simple/select/select';
import Calendar from '../../components/simple/calendar/calendar';
import ButtonQueue from '../../components/simple/button/button';

const CheckIn = () => {

    return(
        <div className={style.container}>
            <Card className={style.card}>
                <form>
                    <div className={style.fieldGroup}>
                        <label className={style.label} htmlFor="first-name">First name</label>
                        <Input id="first-name"></Input>
                    </div>
                    <div className={style.fieldGroup}>
                        <label className={style.label} htmlFor="last-name">Last name</label>
                        <Input id="last-name"></Input>
                    </div>
                    <div className={style.fieldGroup}>
                        <label className={style.label} htmlFor="check-in">Check in</label>
                        <Calendar id="check-in"/>
                    </div>
                    <div className={style.fieldGroup}>
                        <label className={style.label} htmlFor="phone">Phone</label>
                        <Input id="phone"></Input>
                    </div>
                    <div className={style.fieldGroup}>
                        <label className={style.label} htmlFor="address">Address</label>
                        <Input id="address"></Input>
                    </div>
                    <div className={style.fieldGroup}>
                        <label className={style.label} htmlFor="type-service">Service type</label>
                        <SelectQueue id="type-service"/>
                    </div>
                    <ButtonQueue text="Register"/>
                </form>
            </Card>
        </div>
    );
};

export default CheckIn;