import React from 'react';
import Input from '../../components/simple/input/input';
import style from './style.css';
import Calendar from '../../components/simple/calendar/calendar';
import SelectQueue from '../../components/simple/select/select';
import cn from 'classnames'
import ButtonQueue from '../../components/simple/button/button';

const Admin = () => {
    return (
        <div>
            <div className={style.row}>
                <div className={cn(style.fieldGroup, style.margin)}>
                    <label className={style.label} htmlFor="first-name">First name</label>
                    <Input id="first-name"></Input>
                </div>
                <div className={style.fieldGroup}>
                    <label className={style.label} htmlFor="last-name">Last name</label>
                    <Input id="last-name"></Input>
                </div>
            </div>
            <div className={style.row}>
                <div className={cn(style.fieldGroup, style.margin)}>
                    <label className={style.label} htmlFor="phone">Phone</label>
                    <Input id="phone"></Input>
                </div>
                <div className={style.fieldGroup}>
                    <label className={style.label} htmlFor="address">Address</label>
                    <Input id="address"></Input>
                </div>
            </div>
            <div className={style.row}>
                <div className={cn(style.fieldGroup, style.margin)}>
                    <label className={style.label} htmlFor="check-in">Check in</label>
                    <Calendar id="check-in"/>
                </div>
                <div className={style.fieldGroup}>
                    <label className={style.label} htmlFor="type-service">Service type</label>
                    <SelectQueue id="type-service"/>
                </div>
            </div>
            <div className={style.row}>
                <ButtonQueue text="COMPLETE"/>
            </div>
        </div>
    )
};

export default Admin;