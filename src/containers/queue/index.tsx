import React from 'react';

import style from './style.css';
import cn from 'classnames'

import MenuQueue from '../../components/composite/menu';
import Header from '../../components/composite/header';
import Admin from '../../containers/admin';
import CheckIn from '../../containers/check-in/check-in';
import QueueTable from '../../components/simple/table/table';
import { Redirect, Route, Switch } from 'react-router';
import { URLs } from '../../__data__/urls';

const Queue = () => {
    const [open, setOpen] = React.useState(false);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };
    return (
        <div className={style.queueContainerPage}>
            <Header handleDrawerOpen={handleDrawerOpen} open={open}> </Header>
            <MenuQueue handleDrawerClose={handleDrawerClose} open={open}> </MenuQueue>
            <div className={cn(style.content, open && style.contentShift)}>
                <div className={style.drawerHeader}> </div>
                <Switch>
                    <Route exact path="/">
                        <Redirect to={URLs.dashboard.url} />
                    </Route>
                    <Route path={URLs.dashboard.url}>
                        <QueueTable/>
                    </Route>
                    <Route path={URLs.checkIn.url}>
                        <CheckIn/>
                    </Route>
                    <Route path={URLs.admin.url}>
                        <Admin/>
                    </Route>
                    <Route path="*">
                        <h1>Not Found</h1>
                    </Route>
                </Switch>
            </div>
        </div>
    );
}

export default Queue;