import React from 'react';
import Queue from './containers/queue';
import { BrowserRouter } from 'react-router-dom';

const App = () => (
    <BrowserRouter basename='/queue'>
        <Queue />
    </BrowserRouter>
);

export default App;