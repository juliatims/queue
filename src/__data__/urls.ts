import { getNavigations } from '@ijl/cli';

const navigations = getNavigations('queue');

export const URLs = {
    dashboard: {
        url: navigations['link.queue.dashboard'],
    },
    checkIn: {
        url: navigations['link.queue.checkIn'],
    },
    admin: {
        url: navigations['link.queue.admin'],
    },
};