import React from 'react';
import TextField from '@material-ui/core/TextField/TextField';
import style from './style.css';

interface ButtonProps {
    id: string;
}

const Input: React.FC<ButtonProps> = ({id}) => {
    return (
        <TextField className={style.input} id={id} variant="outlined"/>
    );
};

export default Input;