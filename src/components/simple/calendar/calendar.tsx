import React from 'react';
import TextField from '@material-ui/core/TextField/TextField';
import style from './style.css';

interface ButtonProps {
    id: string;
}

const Calendar: React.FC<ButtonProps> = ({id}) => {
    return (
        <TextField
            className={style.calendar}
            id={id}
            variant="outlined"
            type="datetime-local"
        />
    );
};

export default Calendar;