import React from 'react';
import List from '@material-ui/core/List/List';
import ListItem from '@material-ui/core/ListItem/ListItem';
import { Link } from 'react-router-dom';
import style from './style.css';

interface ButtonProps {
    menuItems: Array<{text: string, url: string}>;
}

const ListQueue: React.FC<ButtonProps> = ({menuItems}) => {
    return (
        <List>
            {menuItems.map((item) => (
                <Link className={style.link} to={item.url} key={item.url}>
                    <ListItem button>
                        {item.text}
                    </ListItem>
                </Link>
            ))}
        </List>
    );
};

export default ListQueue;