import React from 'react';
import style from './style.css';
import Button from '@material-ui/core/Button/Button';

interface ButtonProps {
    text: string;
}

const ButtonQueue: React.FC<ButtonProps> = ({text}) => {
    return (
        <Button variant="contained" color="primary">
            {text}
        </Button>
    );
};

export default ButtonQueue;
