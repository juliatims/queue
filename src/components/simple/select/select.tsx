import React from 'react';
import MenuItem from '@material-ui/core/MenuItem/MenuItem';
import Select from '@material-ui/core/Select/Select';
import style from './style.css';

const SelectQueue = ({id}) => {
    return (
        <Select
            className={style.select}
            variant="outlined"
            id={id}
        >
            <MenuItem value="">
                None
            </MenuItem>
        </Select>
    )
};

export default SelectQueue;
