import React from 'react';

import { TableContainer } from '@material-ui/core';
import TableHead from '@material-ui/core/TableHead/TableHead';
import TableRow from '@material-ui/core/TableRow/TableRow';
import TableCell from '@material-ui/core/TableCell/TableCell';
import TableBody from '@material-ui/core/TableBody/TableBody';
import Table from '@material-ui/core/Table/Table';
import style from './style.css';
import Card from '@material-ui/core/Card/Card';

function createData(surname: string, name: string, checkIn: string, status: string) {
    return { surname, name, checkIn, status };
}
const rows = [
    createData('Иванов', 'Иван','13.20', 'На приёме'),
    createData('Федорова', 'Анна', '13:40', 'Ожидает'),
    createData('Петрова', 'Надежда','14.00', 'Ожидает'),
];
const QueueTable = () => {

    return (
        <Card>
            <TableContainer>
                <Table className={style.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Фамилия</TableCell>
                            <TableCell>Имя</TableCell>
                            <TableCell>Время записи</TableCell>
                            <TableCell>Статус</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map((row) => (
                            <TableRow key={row.surname}>
                                <TableCell>{row.surname}</TableCell>
                                <TableCell>{row.name}</TableCell>
                                <TableCell>{row.checkIn}</TableCell>
                                <TableCell>{row.status}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Card>
    );
}

export default QueueTable;