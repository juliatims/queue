import React from 'react';

import { Drawer } from '@material-ui/core';
import style from './style.css';
import ListQueue from '../../simple/list';

interface ButtonProps {
    handleDrawerClose: () => void;
    open: boolean;
}

const MenuQueue: React.FC<ButtonProps> = ({handleDrawerClose, open}) => {
    const menuItems = [{text: 'Dashboard', url: '/dashboard'}, {text: 'Add', url: '/checkIn'}, {text: 'Control', url: '/admin'}];

    return (
        <Drawer
            className={style.drawer}
            variant="persistent"
            anchor="left"
            open={open}
            classes={{
                paper: style.drawerPaper,
            }}
        >
            <div className={style.drawerHeader}>
                <div onClick={handleDrawerClose} className={style.backButton}></div>
            </div>
            <ListQueue menuItems={menuItems}></ListQueue>
        </Drawer>
    );
}

export default MenuQueue;