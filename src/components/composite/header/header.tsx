import React from 'react';

import AppBar from '@material-ui/core/AppBar/AppBar';
import cn from 'classnames'
import style from './header.css';

interface ButtonProps {
    handleDrawerOpen: () => void;
    open: boolean;
}

const Header: React.FC<ButtonProps> = ({ handleDrawerOpen, open }) => {
    return (
        <AppBar className={cn(style.appBar, open && style.appBarShift)}>
            <div className={style.toolbar}>
                <div className={cn(style.barIcon, style.menuButton, open && style.hide)}
                     onClick={handleDrawerOpen}
                ></div>
                <span>Queue</span>
            </div>
        </AppBar>
    );
}

export default Header;